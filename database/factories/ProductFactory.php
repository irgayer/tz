<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $categories = Category::query()->pluck('id')->toArray();
        return [
            'name' => $this->faker->text(20),
            'count' => $this->faker->numberBetween(10, 20),
            'price' => $this->faker->numberBetween(1000, 20000),
            'category_id' => $this->faker->randomElement($categories),
        ];
    }
}
