<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ApiResponser;

    function index(Request $request) {
        return $this->success(Product::filter($request->all())->get(), "List of products");
    }
}
