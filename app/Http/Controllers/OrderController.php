<?php

namespace App\Http\Controllers;

use App\Events\OrderCreated;
use App\Models\Order;
use App\Traits\ApiResponser;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class OrderController extends Controller
{
    use ApiResponser;

    function index(Request $request)
    {
        return $this->success($request->user()->orders()->get(), 'Your orders');
    }

    function create(Request $request)
    {
        $user = $request->user();
        $order = new Order;

        DB::beginTransaction();
        try
        {
            $order = $user->orders()->create();

            foreach ($request->products as $product_id) {
                $order->details()->create(['product_id' => $product_id]);
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            return $this->error(500, "Couldn't create order");
        }

        event(new OrderCreated($order));

        return $this->success(null, 'Order created');
    }
}
