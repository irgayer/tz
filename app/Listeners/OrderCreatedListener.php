<?php

namespace App\Listeners;

use App\Events\OrderCreated;
use App\Mail\OrderCreatedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderCreatedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderCreated $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        $order = $event->order;
        $user = $order->user()->first();

        $mail = new OrderCreatedNotification($user, $order);
        Mail::to($user->email)->send($mail);
        //Log::info($user);
    }
}
