<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCreatedNotification extends Mailable
{
    use Queueable, SerializesModels;

    private $order;
    private $user;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.created')->with(['name' => $this->user->name]);
    }
}
