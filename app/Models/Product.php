<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, Filterable;

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'name', 'count', 'price'
    ];

    public function category() {
        return $this->belongsTo(Category::class);
    }
}
