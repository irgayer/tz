@component('mail::message')
# Hello, {{ $name }}

Thanks for registration.

Best regards,<br>
{{ config('app.name') }}
@endcomponent
