# Тестовое задание
- [Развертывание](#развертывание)
- [API](#api)

## Развертывание 

### Пакеты composer
```bash
composer install
```

### Конфигурация
```bash
touch .env
```
скопируйте .env.example в .env, введите свои данные в 
```dosini
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
и 
```dosini
MAIL_MAILER=
MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=
MAIL_FROM_ADDRESS=
MAIL_FROM_NAME=
```

```bash
php artisan key:generate
```


### База данных
```bash
php artisan migrate
php artisan db:seed
```

### Запуск

_сервер_
```bash
php artisan serve
```
_очередь email_
```bash
php artisan queue:work 
```
_подтверждение заказов_
```bash
php artisan schedule:work 
```
##API

openapi.yaml
открыть [здесь](https://editor.swagger.io)

